//
//  Utility.swift
//  20230314-AdinarayanaBoddapati-NYCSchools
//
//  Created by Aadi on 3/14/23.
//

import UIKit
extension UIViewController {
    
    func showAlert(_ message: String) {
        let cntrl = UIAlertController(title: "OOPS!", message: message, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Okay", style: .default) { action in
            
        }
        cntrl.addAction(okayAction)
        self.present(cntrl, animated: true)
    }
    
}
