//
//  HighSchoolDataViewModel.swift
//  20230314-AdinarayanaBoddapati-NYCSchools
//
//  Created by Aadi on 3/14/23.
//

import Foundation

class HighSchoolDataViewModel {
    var updateUI: ((_ highSchools: [HighSchoolDataModel], _ error: Error?) -> Void)?
    var schoolsData = [HighSchoolDataModel]()
    
    func fetchData() {
        // ["$$app_token": "7pz20kIyizkfXPOu31LOeD7Wv"]
        NetworkManager.shared.request(fromURL: AppURLs.highScoolsAPI, parms: nil) { [weak self] (result: Result<[HighSchoolDataModel], Error>) in
            switch result {
            case .success(let schools):
                self?.schoolsData = schools
                self?.updateUI?(schools, nil)
            case .failure(let error):
                self?.updateUI?([], error)
            }
        }
    }

    func getNumberOfItems() -> Int {
        return schoolsData.count
    }
    
    func getTitleFor(_ indexPath: IndexPath) -> String {
        guard schoolsData[indexPath.row].schoolName.count > 0 else {
            return ""
        }
        return schoolsData[indexPath.row].schoolName
    }
    
    func getDBNFor(_ indexPath: IndexPath) -> String {
        guard schoolsData[indexPath.row].dbn.count > 0 else {
            return ""
        }
        return schoolsData[indexPath.row].dbn
    }
}
